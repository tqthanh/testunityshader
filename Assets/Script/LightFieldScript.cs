﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class LightFieldScript : MonoBehaviour {

	//render using light field material
	public Material mat;
	Texture2D colorTexture; //1024x512 size
	Texture2D depthTexture; //1024x512 size


	void Start(){
		colorTexture = Resources.Load ("color.jpg") as Texture2D;
		depthTexture = Resources.Load ("depth.png") as Texture2D;
	}


	void OnRenderImage(RenderTexture src, RenderTexture  dest){

		mat.SetTexture ("Color Texture", colorTexture);
		mat.SetTexture ("Depth Texture", depthTexture);



		Graphics.Blit (src, dest, mat);

	}
}
