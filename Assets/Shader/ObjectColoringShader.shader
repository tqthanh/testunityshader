﻿Shader "Custom/ObjectColoringShader"
{
	Properties
	{
		_Color("Color", Color) = (0.0,0.0,1.0,1.0)

		_Attrib("an attribute",Float) = 1.0
	}
	SubShader
	{
		Pass
		{
			GLSLPROGRAM

			#include "UnityCG.glslinc"

			uniform lowp vec4 _Color;
			uniform float _Attrib;

			//vertex shader
			#ifdef VERTEX
			void main() {
				gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

			}
			#endif

			//Float _Attrib;

			//fragment shader
			#ifdef FRAGMENT

			

			void main() {
				gl_FragColor = vec4(1.0,0.6,_Attrib,1.0);

				//_Color = vec4(1.0,0.6,0.0,_Attrib);
				//gl_FragColor = _Color;
			}
			#endif



			ENDGLSL

		}
	}
}
