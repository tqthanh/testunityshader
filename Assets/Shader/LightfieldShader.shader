﻿Shader "Custom/LightfieldShader"
{
	Properties
	{
		_colorTexture ("Color Texture",2D) = "white" {}
		_depthTexture ("Depth Texture",2D) = "white" {}
		//_resolution ("Resolution",Vector) = (0,0,0,0)
		//_screenOffset ("Screen Offset",Vector) = (0,0,0,0)
		//_lensWidth ("Lens Width", Float) = 1.27
		//_lensOffset ("Lens Offset", Vector) = (0.0,0.0,0.0,0.0)


	}
	SubShader
	{
		Pass
		{
			GLSLPROGRAM

			#include "UnityCG.cginc"

			//Uniform declaration - same name as properties 
			uniform sampler2D colorTexture;
			uniform sampler2D depthTexture;

			uniform vec2 resolution;
			uniform vec2 screenOffset;  //mm
			uniform float lensWidth;
			uniform vec4 lensOffset;  //mm
			uniform mat3 world_to_zed;
			uniform vec3 zed_plane_ctr;

			// VERTEX SHADER
            #ifdef VERTEX
            void main() {
                gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
            }
            #endif

            #ifdef FRAGMENT

            float4 datax;
          	
            #endif



			ENDGLSL
		}
	}
}
